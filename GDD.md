# Game design document

## 1.0 Title Page   

### 1.1	Game Name  
No concept name so far  

## 2.0	Game Overview
### 2.1.	Game Concept

2D HTML platformer built with PhaserJS. Basic idea is to complete levels by surviving rounds with enemies and time limit.  
Player has a set of abilities they can use. Player progresses by completing rounds and gaining xp and items from each level.  
Enemies scale by player level and get tougher every level.  
The game uses permadeath, so if the player dies, they lose their whole progression.

### 2.2.	Genre

2d survival platformer

### 2.3.	Target Audience

Young audience age of 10 to 25

### 2.4.	Game Flow Summary – How does the player move through the game.   Both through framing interface and the game itself.

The game is in 2D, player moves with WASD+space and abilities up to 5, on number keys 1-5. Player can kill enemies with abilities.
Once player has eliminated all enemies, player must find the exit within the time limit to escape. If player escapes succesfully, player will be rewarded with XP and items.

### 2.5.	Look and Feel – What is the basic look and feel of the game?  What is the visual style?

Overall style is retro rpg. Game has physics and player navigates through platforms and traps. 

## 3.0	Gameplay and  Mechanics
### 3.1.	Gameplay

### 3.1.1.	Game Progression

Player starts with a base character. They have only one attack ability and no items. Player has to survive through first level with them.
Once completed, player returns to "Safe heaven". There player may get new items and upgrade their character and then do other levels. 

Levels scale by player's level, so enemies will be harder to kill and they deal more damage.  
The ultimate goal is to beat final boss at level x, but since permadeath is a thing, player should level up properly first.

#### 3.1.2.	Mission/challenge Structure

There is a small variety of levels that are repeated, but enemies are spawned differently every time. There are spawns around the map and they will be spawning randomly.
Once player has killed x amount of enemies, player must find the exit within the time limit.

#### 3.1.3.	Puzzle Structure

No puzzles are planned so far

#### 3.1.4.	Objectives – What are the objectives of the game?

Survive levels, level up and get items to beat the final level.

#### 3.1.5.	Play Flow – How does the game flow for the game player
### 3.2.	Mechanics – What are the rules to the game, both implicit and explicit.  This is the model of the universe that the game works under.  Think of it as a simulation of a world, how do all the pieces interact?  This actually can be a very large section.
#### 3.2.1.	Physics – How does the physical universe work?
#### 3.2.2.	Movement in the game
#### 3.2.3.	Objects – how to pick them up and move them 
#### 3.2.4.	Actions, including whatever switches and buttons are used, interacting with objects, and what means of communication are used
#### 3.2.5.	Combat – If there is combat or even conflict, how is this specifically modeled?
#### 3.2.6.	Economy – What is the economy of the game? How does it work?
#### 3.2.7.	Screen Flow -- A graphical description of how each screen is related to every other and a description of the purpose of each screen.
### 3.3.	Game Options – What are the options and how do they affect game play and mechanics?
### 3.4.	Replaying and Saving 
### 3.5.	Cheats and Easter Eggs
## 4.0	Story, Setting and Character  
### 4.1.	Story and Narrative – Includes back story, plot elements, game progression, and cut scenes.  Cut scenes descriptions include the actors, the setting, and the storyboard or script.
### 4.2.	Game World
#### 4.2.1.	General look and feel of world
#### 4.2.2.	Areas, including the general description and physical characteristics as well as how it relates to the rest of the world (what levels use it, how it connects to other areas)
### 4.3.	Characters.  Each character should include the back story, personality, appearance, animations, abilities, relevance to the story and relationship to other characters 
## 5.	Levels
### 5.1.	Levels.  Each level should include a synopsis, the required introductory material (and how it is provided), the objectives, and the details of what happens in the level.  Depending on the game, this may include the physical description of the map, the critical path that the player needs to take, and what encounters are important or incidental.
### 5.2.	Training Level
## 6.	Interface
### 6.1.	Visual System.  If you have a HUD, what is on it?  What menus are you displaying? What is the camera model?
### 6.2.	Control System – How does the game player control the game?   What are the specific commands?
### 6.3.	Audio, music, sound effects
### 6.4.	Help System
## 7.	Artificial Intelligence
### 7.1.	Opponent  and Enemy AI – The active opponent that plays against the game player and therefore requires strategic decision making 
### 7.2.	Non-combat and Friendly Characters
### 7.3.	Support AI -- Player and Collision Detection, Pathfinding
## 8.	Technical 
### 8.1.	Target Hardware

This is a light HTML-ready game for browsers, so any PC will do.

### 8.2.	Development hardware and software, including Game Engine

We are using PhaserJS game engine, programming with Javascript in Visual studio code

### 8.3.	Network requirements

The game will be hosted on a server, so internet connection is required to play on a browser. Saving is made using cookies.
The game doesn't use much bandwidth, so fast internet speed is not required.

## 9.	Game Art – Key assets, how they are being developed.  Intended style.
