//Reference for game object, we use it to attach stages(maps) to the engine
var rpg = {};

//Another game object, but this is for Inventory that is rendered on a different canvas
var rpgInv = {};

//Default horizontal movement speed
var xMovementSpeed = 300;
//Jump speed 
var yMovementSpeed = 550;

var xFraction = 35;
var xyFraction = 50;


//Inventory
rpgInv.state0 = function(){};
rpgInv.state0.prototype = {
    preload: function(){
        inventory.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        inventory.load.tilemap('inventoryMap', 'assets/tilemaps/inventory/inventory.json', null, Phaser.Tilemap.TILED_JSON);
        inventory.load.image('inventoryBG', 'assets/inventory/inventorybg.png');


        inventory.load.image('inventoryButton', 'assets/inventory/inventoryButton.png');
        inventory.load.image('equipmentButton', 'assets/inventory/equipmentButton.png');

        inventory.load.image('equipmentBG', 'assets/inventory/equipmentbg.png');
        inventory.load.tilemap('equipmentMap', 'assets/tilemaps/inventory/equipment.json', null, Phaser.Tilemap.TILED_JSON);

        loadAssets(inventory);
    },
    create: function(){
        inventoryItems = inventory.add.group();
        inventoryItems.enableBody = true;
        inventoryItems.inputEnableChildren = true;

        equipmentSprites = inventory.add.group();
        equipmentSprites.enableBody = true;
        equipmentSprites.inputEnableChildren = true;
        
        inventorySlots = inventory.add.group();
        equipmentSlots = inventory.add.group();

        //inventory.stage.backgroundColor = "#4488AA";
        inventoryState = "inventory";
  
        var inventoryImage = inventory.add.image(0,0,'inventoryBG');
        inventory.world.sendToBack(inventoryImage);    

        var equipmentImage = inventory.add.image(0,0,'equipmentBG');
        inventory.world.sendToBack(equipmentImage);  

        //Map loading
        var map = inventory.add.tilemap('inventoryMap');
    
        // Get object layers from the tileset and turn them into objects in phaser, so we can use them as sprites 
        map.createFromObjects('slots', 1, '', 0, true, false, inventorySlots); 
        
        var map2 = inventory.add.tilemap('equipmentMap');

        map2.createFromObjects('equipmentSlots', 1, '', 0, true, false, equipmentSlots);
    
        if(saveExists && loadedInventory != undefined)
        {
            loadInventory(loadedInventory);
        }
        if(saveExists && loadedEquipment != undefined)
        {
            loadEquipment(loadedEquipment);
        }        
        var button1 = inventory.add.button(250,200, 'inventoryButton', function(){
            console.log('1');
            inventory.world.sendToBack(equipmentImage);
            inventoryState = "inventory";

            equipmentSprites.callAll('kill');
            inventoryItems.callAll('revive');
            updateStats();

        }) 

        var button2 = inventory.add.button(30,200, 'equipmentButton', function(){
            console.log('2');
            inventory.world.sendToBack(inventoryImage);
            inventoryState = "equipment";

            equipmentSprites.callAll('revive');
            inventoryItems.callAll('kill');
            updateStats();
        })

    },
    update: function(){
        equipmentSprites.onChildInputDown.add(unEquip, this);
        inventoryItems.onChildInputDown.add(itemClicked, this);
        
    }
};

//Mainmenu
rpg.state0 = function(){};
rpg.state0.prototype = {
    preload: function(){


    },
    create: function(){
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.world.setBounds(0, 0, 1920, 800 );
        
    
    },
    update: function(){
                /* RESOURCEBAR MONITORING*/
                if(player.Alive){
                    //Healthbar update
                    game.myHealthBar.setPercent(player.Health*100/player.maxHealth);
                    renderHealthnumber.text = player.Health + "/" + player.maxHealth;
                
                    //XPbar update
                    game.XPBar.setPercent(player.XP*100/XlevelXP);
                    renderXPnumber.text = player.XP;
                }
    }
};

//Debug
rpg.state1 = function(){};

rpg.state1.prototype = {
    preload: function(){
        loadAssets(game);

        //Reset temporary variables
        resetVariables();       
        
        if(!inventoryOn)
        {
            inventory.state.start('inventory');
            inventoryOn = true;
        }
    },
    create: function(){
        //Set scaling mode, so the game fits different sized screens
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        //Set world bounds, so player can't fall out of the map
        game.world.setBounds(0, 0, 1920, 928 );

        //Allow the player use arrow keys for input
        cursors = game.input.keyboard.createCursorKeys();

        //Map laoding
        var map = game.add.tilemap('basicdebug');
        map.addTilesetImage('grass');

        platforms = map.createLayer('platform');

        map.setCollisionBetween(0, 1 ,true, 'platform');

        //Load parent groups so we can create child elements with same attributes easier
        loadObjectGroups();

        // Get object layers from the tileset and turn them into objects in phaser, so we can use them as sprites 
        map.createFromObjects('spawn', 2, '', 0, true, false, spawnPoints); 

        //Let's find the index for player spawn from the spawn point array
        var playerSpawn = spawnPoints.children.findIndex(function(point)
        {
            return point.name == 'playerSpawn';
        })

        //Let's find the index for player exit from the spawn point array
        var playerExit = spawnPoints.children.findIndex(function(point)
        {
            return point.name == 'playerExit';
        })

        //Put exit points in variables so we can spawn exits to these coordinates later on
        exitX = spawnPoints.children[playerExit].x;
        exitY = spawnPoints.children[playerExit].y;

        //Create player, give it attributes and place it on the game
        createCharacter(spawnPoints.children[playerSpawn].x, spawnPoints.children[playerSpawn].y);

        //Enemy spawning
        for(var i = 1; i <= 10; i++)
        {
            var index = spawnPoints.children.findIndex(function(point)
            {
                return point.name == 'enemySpawn' + i;
            }) 
            if(index == -1){break;}
            else{
                enemySpawns.push(spawnPoints.children[index]);
            }
        }

        enemyWaves = 0;
        spawnEnemies(enemySpawns.length, enemySpawns);

        //Load UI elements for abilities, score etc
        loadUIelements();

        for(var i = 1; i < 6; i++)
        {
            var items = ["redstaff" , "purplestaff", "yellowstaff", "bluestaff", 'sword1'];
            var randomNumber = Math.floor(Math.random()*items.length);
            var item = "item"+itemCounter;
            item = lootableItems.create(100 + i * 100, 800, items[randomNumber]);
            item.name = "item"+itemCounter;
            if(items[randomNumber] == "sword1")
            {
                item.type = "weapon";
                item.magic = Math.floor(Math.random() * 3) + 1;
                item.damage = Math.floor(Math.random() * 10) + 1;
            }
            else{
                item.type = "weapon";
                item.damage = Math.floor(Math.random() * 3) + 1;
                item.magic = Math.floor(Math.random() * 10) + 1;
            }
            
            itemCounter++;
        }

        for(var i = 1; i < 6; i++)
        {
            var items = ["leatherBoots", "leatherPants", "leatherChest"];
            var randomNumber = Math.floor(Math.random()*items.length);
            var item = "item"+itemCounter;
            item = lootableItems.create(600 + i * 100, 800, items[randomNumber]);
            item.name = "item"+itemCounter;
            item.armor = Math.floor(Math.random() * 10) + 1;
            if(items[randomNumber] == "leatherChest")
            {
                item.type = "chest";
            }
            else if(items[randomNumber] == "leatherPants")
            {
                item.type = "pants";
            }
            else if(items[randomNumber] == "leatherBoots")
            {
                item.type = "boots";
            }
            
            itemCounter++;
        }

       
    },
    update: function(){
        /*Colliders*/
        game.physics.arcade.collide(player, platforms); 
        game.physics.arcade.collide(enemies, platforms); 
        game.physics.arcade.overlap(player, exitPoints, levelFinished, null, this);

        game.physics.arcade.collide(lootableItems, platforms); 
        //game.physics.arcade.overlap(player, lootableItems, autoLoot, null, this);
    

        if(game.physics.arcade.collide(playerShots, platforms))
        {
            playerShots.callAll("kill");
        }

        lootableItems.onChildInputDown.add(itemPickUp, this);

        game.physics.arcade.overlap(enemies, playerShots, enemyHit, null ,this)

        /* MOVEMENT SCRIPTS */
        //  Reset or modify when there's no input, like friction

        //player.body.velocity.x = 0;
        //While moving to right
        if(player.body.velocity.x > 100)
        {
            player.body.velocity.x -= xFraction;
        }
        //When moving to right and while in air
        else if(player.body.velocity.x > 0 && player.body.velocity.y < 0 )
        {
            player.body.velocity.x -= xyFraction;
        }
        //While moving to left
        else if(player.body.velocity.x < 0)
        {
            player.body.velocity.x += xFraction;
        }
        //When moving to left and while in air
        else if(player.body.velocity.x < 0 && player.body.velocity.y < 0)
        {
            player.body.velocity.x += xyFraction;
        }

        //When pressing right
        if (cursors.right.isDown || game.input.keyboard.isDown(moveRight))
        {
            player.body.velocity.x = xMovementSpeed;

            player.animations.play('right');
            
            lastDir = 2; // 0 for idle, 1 for left, 2 for right
        }    
        //When pressing left
        else if (cursors.left.isDown|| game.input.keyboard.isDown(moveLeft))
        {
            player.body.velocity.x = -xMovementSpeed;

            player.animations.play('left');

            lastDir = 1; // 0 for idle, 1 for left, 2 for right
        } 
        //When pressing down
        else if (cursors.down.isDown || game.input.keyboard.isDown(moveDown))
        {
                player.body.velocity.x = 0;    
        }
        //Where's no input
        else
        {
            //  Stand still
            player.animations.stop();
            if(lastDir == 1){
                player.frame = 2;
            }
            else if(lastDir == 2)
                {
                    player.frame = 7;
                }
            else {
                    player.frame = 4;
            }
        }
        //  Allow the player to jump if they are touching the ground.
        if (cursors.up.isDown && player.body.blocked.down || game.input.keyboard.isDown(moveUp) && player.body.blocked.down || game.input.keyboard.isDown(moveUp2) && player.body.blocked.down)
        {
            player.body.velocity.y = -yMovementSpeed;
                    
        }

        if(game.input.keyboard.isDown(shift))
        {
            player.animations.stop();
            player.frame = 4;
            player.body.velocity.y = 0;
            player.body.velocity.x = 0;
        }
        /*End of movement scripts*/

        /* RESOURCEBAR MONITORING*/
        if(player.Alive){
            //Healthbar update
            game.myHealthBar.setPercent(player.Health*100/player.maxHealth);
            renderHealthnumber.text = player.Health + "/" + player.maxHealth;
        
            //XPbar update
            game.XPBar.setPercent(player.XP*100/XlevelXP);
            renderXPnumber.text = player.XP;
        }

        
        /* PLAYER ABILITIES */
        
        // Use shooting skill when hotkey is pressed
        if(game.input.keyboard.isDown(shootHotkey) && player.Alive)
        {
            fireBall();

        }

        /* ENEMY AI */

        for(var i = 0; i < enemyList.length; i++)
        {
            if(enemyList[i].name.Alive)
            {
                enemyList[i].Patrol();
            }
        }
        

        /* MAP PROGRESS */

        //Player fails the round and dies.
        if(gameOver){
            GameOver();
        }

        //When the objective is completed, spawn an exit
        if(objectiveCompleted && !exitSpawned){
            spawnExit(exitX, exitY);
            exitSpawned = true;
        }
        //End of update scripts
    }
};

//Safeland
rpg.state2 = function(){};

rpg.state2.prototype = {
    preload: function(){
        //Load all assets, like images and tilemaps
        loadAssets(game);

        //Reset temporary variables
        resetVariables();        
        
        if(!inventoryOn)
        {
            inventory.state.start('inventory');
            inventoryOn = true;
        }
    },
    create: function(){
        //Set scaling mode, so the game fits different sized screens
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        //Set world bounds, so player can't fall out of the map
        game.world.setBounds(0, 0, 1920, 928 );

        //Allow the player use arrow keys for input
        cursors = game.input.keyboard.createCursorKeys();

        //Map loading
        //Create a map object
        var map = game.add.tilemap('safeland');

        //Load images for tiles so phaser can render them (change to your images)
        map.addTilesetImage('houseDark');
        map.addTilesetImage('houseLeft');
        map.addTilesetImage('houseRight');
        map.addTilesetImage('houseTop');
        map.addTilesetImage('houseTopLeft');
        map.addTilesetImage('houseTopRight');
        map.addTilesetImage('roundRoad');
        map.addTilesetImage('houseDarkAlt');
        map.addTilesetImage('houseDarkAlt2');

        //Tilemap layers
        var background = map.createLayer('background');
        platforms = map.createLayer('platform');

        map.setCollisionBetween(10, 20,true, 'platform');

        //Loads all Phaser Object Groups, this lets us to create child elements into pre-made templates
        //This is needed before loading createFromObjects, since those objects are placed into ObjectGroups
        loadObjectGroups();

        // Get object layers from the tileset and turn them into objects in phaser, so we can use them as sprites 
        map.createFromObjects('spawn', 26, '', 0, true, false, spawnPoints); 
        
        //Create player, give it attributes and place it on the game
        createCharacter(spawnPoints.children[0].x, spawnPoints.children[0].y);
        
        player.body.gravity.y = 0;

        //Load UI elements for abilities, score etc
        loadUIelements();


        for(var i = 1; i < 10; i++)
        {
            var Index = spawnPoints.children.findIndex(function(point)
            {
                return point.name == "mapDoor" + i;
            })
            
            if(Index == -1){break;}
            else{
                var doorName = "door" + i;
                doorName = exitPoints.create(spawnPoints.children[Index].x, spawnPoints.children[Index].y, 'window');
                doorName.immovable = true;
                doorName.target = spawnPoints.children[Index].target;
            }
        }
        
        
    },
    update: function(){
        /*Colliders*/
        game.physics.arcade.collide(player, platforms); 
        game.physics.arcade.overlap(player, exitPoints, function(player, exitPoints){changeLevel(exitPoints.target)}, null, this);

        game.physics.arcade.collide(lootableItems, platforms); 
        //game.physics.arcade.overlap(player, lootableItems, collectItem, null, this);
        lootableItems.onChildInputDown.add(itemPickUp, this);

        lootableItems.setAll('body.velocity.y', 0);

        /* MOVEMENT SCRIPTS */
        //  Reset or modify when there's no input, like friction

        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        //While moving to right

        //When pressing right
        if (cursors.right.isDown || game.input.keyboard.isDown(moveRight))
        {
            player.body.velocity.x = 300;

            player.animations.play('right');
            
            lastDir = 2; // 0 for idle, 1 for left, 2 for right
        }

        //When pressing left
        else if (cursors.left.isDown|| game.input.keyboard.isDown(moveLeft))
        {
            player.body.velocity.x = -300;

            player.animations.play('left');

            lastDir = 1; // 0 for idle, 1 for left, 2 for right
        }
        //Where's no input
        else
        {
            //  Stand still
            player.animations.stop();
            if(lastDir == 1){
                player.frame = 2;
            }
            else if(lastDir == 2)
                {
                    player.frame = 7;
                }
            else {
                    player.frame = 4;
            }
        }                
        
        //When pressing up
        if (cursors.up.isDown || game.input.keyboard.isDown(moveUp2))
        {
            player.body.velocity.y = -300;    
        }
        //When pressing down
        else if (cursors.down.isDown || game.input.keyboard.isDown(moveDown))
        {
                player.body.velocity.y = 300;    
        }

        /*End of movement scripts*/

        /* RESOURCEBAR MONITORING*/
        if(player.Alive){
            //Healthbar update
            game.myHealthBar.setPercent(player.Health*100/player.maxHealth);
            renderHealthnumber.text = player.Health + "/" + player.maxHealth;
        
            //XPbar update
            game.XPBar.setPercent(player.XP*100/XlevelXP);
            renderXPnumber.text = player.XP;
        }

    }
};

//Area2
rpg.state3 = function(){};

rpg.state3.prototype = {
preload: function(){
        loadAssets(game);
        
        //Reset temporary variables
        resetVariables(); 
        
        if(!inventoryOn)
        {
            inventory.state.start('inventory');
            inventoryOn = true;
        }
},
create: function(){
//Set scaling mode, so the game fits different sized screens
game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

//Set world bounds, so player can't fall out of the map
game.world.setBounds(0, 0, 1920, 928 );

//Allow the player use arrow keys for input
cursors = game.input.keyboard.createCursorKeys();

//Map laoding
var map = game.add.tilemap('area2');
map.addTilesetImage('grass');
map.addTilesetImage('houseDark');
map.addTilesetImage('houseLeft');
map.addTilesetImage('houseRight');
map.addTilesetImage('houseTop');
map.addTilesetImage('houseTopLeft');
map.addTilesetImage('houseTopRight');

var background = map.createLayer('background');
platforms = map.createLayer('platform');

map.setCollisionBetween(0, 1 ,true, 'platform');

//Loads all Phaser Object Groups, this lets us to create child elements into pre-made templates
//This is needed before loading createFromObjects, since those objects are placed into ObjectGroups
loadObjectGroups();

// Get object layers from the tileset and turn them into objects in phaser, so we can use them as sprites 
map.createFromObjects('spawn', 26, '', 0, true, false, spawnPoints); 

//Let's find the index for player spawn from the spawn point array
var playerSpawn = spawnPoints.children.findIndex(function(point)
{
    return point.name == 'playerSpawn';
})

//Let's find the index for player exit from the spawn point array
var playerExit = spawnPoints.children.findIndex(function(point)
{
    return point.name == 'playerExit';
})

//Put exit points in variables so we can spawn exits to these coordinates later on
exitX = spawnPoints.children[playerExit].x;
exitY = spawnPoints.children[playerExit].y;

//Create player, give it attributes and place it on the game
createCharacter(spawnPoints.children[playerSpawn].x, spawnPoints.children[playerSpawn].y);

//Enemy spawning
for(var i = 1; i <= 10; i++)
    {
    var index = spawnPoints.children.findIndex(function(point)
    {
        return point.name == 'enemySpawn' + i;
    }) 
    if(index == -1){break;}
    else{
        enemySpawns.push(spawnPoints.children[index]);
    }
}

enemyWaves = 3;
spawnEnemies(3, spawnPoints);

//Load UI elements for abilities, score etc
loadUIelements();

},
update: function(){
/*Colliders*/
game.physics.arcade.collide(player, platforms); 
game.physics.arcade.collide(enemies, platforms); 
game.physics.arcade.overlap(player, exitPoints, levelFinished, null, this);

game.physics.arcade.collide(lootableItems, platforms); 
//game.physics.arcade.overlap(player, lootableItems, collectItem, null, this);
lootableItems.onChildInputDown.add(itemPickUp, this);

if(game.physics.arcade.collide(playerShots, platforms))
{
    playerShots.callAll("kill");
}

game.physics.arcade.overlap(enemies, playerShots, enemyHit, null ,this)

/* MOVEMENT SCRIPTS */
//  Reset or modify when there's no input, like friction

//player.body.velocity.x = 0;
//While moving to right
if(player.body.velocity.x > 100)
{
    player.body.velocity.x -= xFraction;
}
//When moving to right and while in air
else if(player.body.velocity.x > 0 && player.body.velocity.y < 0 )
{
    player.body.velocity.x -= xyFraction;
}
//While moving to left
else if(player.body.velocity.x < 0)
{
    player.body.velocity.x += xFraction;
}
//When moving to left and while in air
else if(player.body.velocity.x < 0 && player.body.velocity.y < 0)
{
    player.body.velocity.x += xyFraction;
}

//When pressing right
if (cursors.right.isDown || game.input.keyboard.isDown(moveRight))
{
    player.body.velocity.x = xMovementSpeed;

    player.animations.play('right');
    
    lastDir = 2; // 0 for idle, 1 for left, 2 for right
}    
//When pressing left
else if (cursors.left.isDown|| game.input.keyboard.isDown(moveLeft))
{
    player.body.velocity.x = -xMovementSpeed;

    player.animations.play('left');

    lastDir = 1; // 0 for idle, 1 for left, 2 for right
} 
//When pressing down
else if (cursors.down.isDown || game.input.keyboard.isDown(moveDown))
{
        player.body.velocity.x = 0;    
}
//Where's no input
else
{
    //  Stand still
    player.animations.stop();
    if(lastDir == 1){
        player.frame = 2;
    }
    else if(lastDir == 2)
        {
            player.frame = 7;
        }
    else {
            player.frame = 4;
    }
}
//  Allow the player to jump if they are touching the ground.
if (cursors.up.isDown && player.body.blocked.down || game.input.keyboard.isDown(moveUp) && player.body.blocked.down || game.input.keyboard.isDown(moveUp2) && player.body.blocked.down)
{
    player.body.velocity.y = -500;
            
}

if(game.input.keyboard.isDown(shift))
{
    player.animations.stop();
    player.frame = 4;
    player.body.velocity.y = 0;
    player.body.velocity.x = 0;
}
/*End of movement scripts*/

/* RESOURCEBAR MONITORING*/
if(player.Alive){
    //Healthbar update
    game.myHealthBar.setPercent(player.Health*100/player.maxHealth);
    renderHealthnumber.text = player.Health + "/" + player.maxHealth;

    //XPbar update
    game.XPBar.setPercent(player.XP*100/XlevelXP);
    renderXPnumber.text = player.XP;
}

/* PLAYER ABILITIES */

// Use shooting skill when hotkey is pressed
if(game.input.keyboard.isDown(shootHotkey) && player.Alive)
{
    fireBall();

}

/* ENEMY AI */

for(var i = 0; i < enemyList.length; i++)
{
    if(enemyList[i].name.Alive)
    {
        enemyList[i].Patrol();
    }
}

/* MAP PROGRESS */

//Player fails the round and dies.
if(gameOver){
    GameOver();
}

//When the objective is completed, spawn an exit
if(objectiveCompleted && !exitSpawned){
    spawnExit(exitX, exitY);
    exitSpawned = true;
}
//End of update scripts
}
};

//Area3
rpg.state4 = function(){};

rpg.state4.prototype = {
    preload: function(){
        //Load all assets, like images and tilemaps
        loadAssets(game);

        //Reset temporary variables
        resetVariables();   

        if(!inventoryOn)
        {
            inventory.state.start('inventory');
            inventoryOn = true;
        }
    },
    create: function(){
        //Set scaling mode, so the game fits different sized screens
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        //Set world bounds, so player can't fall out of the map
        game.world.setBounds(0, 0, 1920, 928 );

        //Allow the player use arrow keys for input
        cursors = game.input.keyboard.createCursorKeys();

        //Map loading
        //Create a map object
        var map = game.add.tilemap('area3');

        //Load images for tiles so phaser can render them (change to your images)
        map.addTilesetImage('grass');
        map.addTilesetImage('houseDark');
        map.addTilesetImage('houseLeft');
        map.addTilesetImage('houseRight');
        map.addTilesetImage('houseTop');
        map.addTilesetImage('houseTopLeft');
        map.addTilesetImage('houseTopRight');

        var background = map.createLayer('background');
        platforms = map.createLayer('platform');

        //Tilemap layers
        //var background = map.createLayer('background');

        //Loads all Phaser Object Groups, this lets us to create child elements into pre-made templates
        //This is needed before loading createFromObjects, since those objects are placed into ObjectGroups
        loadObjectGroups();

        map.setCollisionBetween(0, 1 ,true, 'platform');

        // Get object layers from the tileset and turn them into objects in phaser, so we can use them as sprites 
        map.createFromObjects('spawn', 26, '', 0, true, false, spawnPoints);        

        //Let's find the index for player spawn from the spawn point array
        var playerSpawn = spawnPoints.children.findIndex(function(point)
        {
            return point.name == 'playerSpawn';
        })

        //Let's find the index for player exit from the spawn point array
        var playerExit = spawnPoints.children.findIndex(function(point)
        {
            return point.name == 'playerExit';
        })

        //Put exit points in variables so we can spawn exits to these coordinates later on
        exitX = spawnPoints.children[playerExit].x;
        exitY = spawnPoints.children[playerExit].y;

        //Create player, give it attributes and place it on the game
        createCharacter(spawnPoints.children[playerSpawn].x, spawnPoints.children[playerSpawn].y);
        //Enemy spawning
        for(var i = 1; i <= 10; i++)
            {
            var index = spawnPoints.children.findIndex(function(point)
            {
                return point.name == 'enemySpawn' + i;
            }) 
            if(index == -1){break;}
            else{
                enemySpawns.push(spawnPoints.children[index]);
            }
        }

        enemyWaves = 3;
        spawnEnemies(3, spawnPoints);

        //Load UI elements for abilities, score etc
        loadUIelements();

    },
    update: function(){
        /*Colliders*/
        game.physics.arcade.collide(player, platforms); 
        game.physics.arcade.collide(enemies, platforms); 
        game.physics.arcade.overlap(player, exitPoints, levelFinished, null, this);

        game.physics.arcade.collide(lootableItems, platforms); 
        //game.physics.arcade.overlap(player, lootableItems, collectItem, null, this);
        lootableItems.onChildInputDown.add(itemPickUp, this);
        
        if(game.physics.arcade.collide(playerShots, platforms))
        {
            playerShots.callAll("kill");
        }

        game.physics.arcade.overlap(enemies, playerShots, enemyHit, null ,this)

        /* MOVEMENT SCRIPTS */
        //  Reset or modify when there's no input, like friction

        //player.body.velocity.x = 0;
        //While moving to right
        if(player.body.velocity.x > 100)
        {
            player.body.velocity.x -= xFraction;
        }
        //When moving to right and while in air
        else if(player.body.velocity.x > 0 && player.body.velocity.y < 0 )
        {
            player.body.velocity.x -= xyFraction;
        }
        //While moving to left
        else if(player.body.velocity.x < 0)
        {
            player.body.velocity.x += xFraction;
        }
        //When moving to left and while in air
        else if(player.body.velocity.x < 0 && player.body.velocity.y < 0)
        {
            player.body.velocity.x += xyFraction;
        }

        //When pressing right
        if (cursors.right.isDown || game.input.keyboard.isDown(moveRight))
        {
            player.body.velocity.x = xMovementSpeed;

            player.animations.play('right');
            
            lastDir = 2; // 0 for idle, 1 for left, 2 for right
        }    
        //When pressing left
        else if (cursors.left.isDown|| game.input.keyboard.isDown(moveLeft))
        {
            player.body.velocity.x = -xMovementSpeed;

            player.animations.play('left');

            lastDir = 1; // 0 for idle, 1 for left, 2 for right
        } 
        //When pressing down
        else if (cursors.down.isDown || game.input.keyboard.isDown(moveDown))
        {
                player.body.velocity.x = 0;    
        }
        //Where's no input
        else
        {
            //  Stand still
            player.animations.stop();
            if(lastDir == 1){
                player.frame = 2;
            }
            else if(lastDir == 2)
                {
                    player.frame = 7;
                }
            else {
                    player.frame = 4;
            }
        }
        //  Allow the player to jump if they are touching the ground.
        if (cursors.up.isDown && player.body.blocked.down || game.input.keyboard.isDown(moveUp) && player.body.blocked.down || game.input.keyboard.isDown(moveUp2) && player.body.blocked.down)
        {
            player.body.velocity.y = -500;
                    
        }

        if(game.input.keyboard.isDown(shift))
        {
            player.animations.stop();
            player.frame = 4;
            player.body.velocity.y = 0;
            player.body.velocity.x = 0;
        }
        /*End of movement scripts*/

        /* RESOURCEBAR MONITORING*/
        if(player.Alive){
            //Healthbar update
            game.myHealthBar.setPercent(player.Health*100/player.maxHealth);
            renderHealthnumber.text = player.Health + "/" + player.maxHealth;
        
            //XPbar update
            game.XPBar.setPercent(player.XP*100/XlevelXP);
            renderXPnumber.text = player.XP;
        }
        
        /* PLAYER ABILITIES */
        
        // Use shooting skill when hotkey is pressed
        if(game.input.keyboard.isDown(shootHotkey) && player.Alive)
        {
            fireBall();

        }

        /* ENEMY AI */

        for(var i = 0; i < enemyList.length; i++)
        {
            if(enemyList[i].name.Alive)
            {
                enemyList[i].Patrol();
            }
        }


        /* MAP PROGRESS */

        //Player fails the round and dies.
        if(gameOver){
            GameOver();
        }

        //When the objective is completed, spawn an exit
        if(objectiveCompleted && !exitSpawned){
            spawnExit(exitX, exitY);
            exitSpawned = true;
        }
        //End of update scripts
    }
};