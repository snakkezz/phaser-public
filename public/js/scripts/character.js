function createCharacter(posX, posY){
    // Create a new player sprite
    player = game.add.sprite(posX, posY, 'hero');
        
    //  We need to enable physics on the player
    game.physics.arcade.enable(player);
        
    //  Player physics properties.
    player.body.bounce.y = 0.1;
    player.body.gravity.y = 1000;
        
    //Make the player collide with world bounds, so they don't fall out of the map
    player.body.collideWorldBounds = true;

    //  Our two animations, walking left and right.
    player.animations.add('left', [0, 1, 2, 3], 10, true);
    player.animations.add('right', [5, 6, 7, 8], 10, true);
        
    // Camera movement
    game.camera.follow(player);
            
    game.camera.deadzone = new Phaser.Rectangle(600, 300, 100, 200); // x, y position - width / height

    playerStats = [];

    //Check if a game save exists in cookies
    checkCookie();
    
    //If game save exists, load it
    if(saveExists)
    {
        loadGame();
        setPlayerProperties(playerStats);
    }
    else{
        //If not, load a new characher
        setPlayerProperties();
    }

}
//After the base character has been created, check if save exists and load it, otherwise skip to setPlayerProperties

function setPlayerProperties(playerStats){
    //New character stats
    if(playerStats == null)
    {
        console.log('New character loaded');
        player.XP = 0;
        player.level = 1;
        player.skillPoints = 0;     //Skillpoints that are acquired by leveling up
        player.type = "Warrior";

        player.Health = 5;
        player.armor = 2;
        player.resistance = 2;

        player.damage = 10;
        player.magic = 10;
        
        //Game variables for other functions
        lastDir = 2;
        player.maxHealth = player.Health;
        player.Alive = true;
        score = 0;

        //Set variables for healthbar
        XlevelXP = player.level * 100 + (player.level * 25);
        tillNextLVL = countDifference(XlevelXP,  0);
    
    }
    else{
        console.log('Game save loaded')
        
        player.XP = JSON.parse(playerStats[0]);
        player.level = JSON.parse(playerStats[1]);
        player.skillPoints = JSON.parse(playerStats[2])
        player.type = playerStats[3]; //Player class
            
        player.Health = JSON.parse(playerStats[4]);
        player.armor = JSON.parse(playerStats[5]);
        player.resistance = JSON.parse(playerStats[6]);
        player.damage = JSON.parse(playerStats[7]);
        player.magic = JSON.parse(playerStats[8]);
        player.maxHealth = JSON.parse(playerStats[9]);
        score = JSON.parse(playerStats[10]);

        //Game variables for other functions
        lastDir = 2;
        player.Alive = true;

        //Set variables for healthbar
        XlevelXP = Math.pow(player.level,2) * 100 + (player.level * 25);
        tillNextLVL = countDifference(XlevelXP,  0);
    }

}

