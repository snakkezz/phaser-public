class Enemy{
    constructor(name,Level,posx,posy,sprite)
    {        
        this.name = name;
        // Create a new child to enemies group
        this.name = enemies.create(posx,posy,sprite);
        game.physics.arcade.enable(this.name); 

        this.name.name = "enemy" + enemyID;
        this.name.level = Level;
        this.name.Health = 5+2*this.name.level;
        this.name.XP = 60 * this.name.level + 25 * this.name.level;
        this.name.damage = 2+2*this.name.level;
        this.name.armor = 1+1.5*this.name.level;
        this.name.resistance = 1+1.25*this.name.level;
        
        this.name.Alive = true;
        this.name.body.gravity.y = 100;
        this.name.body.velocity.x = 100;
        
        this.name.animations.add('left', [0, 1, 2, 3], 8, true);
        this.name.animations.add('right', [5, 6, 7, 8], 8, true);

        this.name.animations.play('right');
        enemyID++;
    }
    Patrol()
    {
        var direction;
        if(this.name.body.velocity.x > 0)
            {
                direction = "right";
            }
        else
            {
                direction = "left";
            }
        
        if(this.name.body.blocked.right)
            {
                this.name.animations.play('left');
                direction = "left";
            }
        if(this.name.body.blocked.left)
            {
                this.name.animations.play('right');
                direction = "right";
            }
        if(this.name.body.blocked.down == false)
            {
                if(direction == "right")
                    {
                        this.name.body.x -= 15;
                        this.name.animations.play('left');
                        direction = "left";
                    }
                else{
                        this.name.body.x += 15;
                        this.name.animations.play('right');
                        direction = "right";
                }
            }
        
        if(direction == "right")
            {
                this.name.body.velocity.x = 100;
            }
        else if(direction == "left")
            {
                this.name.body.velocity.x = -100;
            }
    }
}

function enemyHit(enemy, playerShot){

    if(playerShot.key == "fireball")
    {
    playerShot.kill();
    var damage = player.magic;

    //Damage is counted from Player's magic damage minus enemy's resistance
    var preMigitation = damage-enemy.resistance;

    //If damage is smaller or equal to 0, make it 1
    if(preMigitation <= 0)
        {
            preMigitation = 1;
        }
    //Round up/down the damage for clearer numbers
    else{
            preMigitation = Math.round(preMigitation);
    }

        //Reduce enemy health
        enemy.Health = enemy.Health-preMigitation;

        //Create damage numbers above the enemy
        var damageText = game.add.text(enemy.body.x, enemy.body.y, '-' + preMigitation, { fontSize: '10px', fill: '#FFFFFF' });
        game.physics.arcade.enable(damageText);
        damageText.body.velocity.y = -10;

        game.time.events.add(Phaser.Timer.SECOND * 0.5, function() {damageText.alpha = 0;}, this);


        //If the enemy dies, kill the sprite and remove it from enemyList
        if(enemy.Health <= 0)
        {              
            //Let's find the index of the enemy object from enemyLIST
            var enemyIndex = enemyList.findIndex(function(point)
            {
                return point.name.name == enemy.name;
            })     

            //Kill the enemy and remove it from the game
            enemy.Alive = false;   
            enemy.kill();

            //If enemies are left, reduce the count and update UI text
            if(enemyList.length > 1)
            {
                enemyList.splice(enemyIndex, 1);
                enemyCountText.text = 'Enemies left: ' + enemyList.length;

            }
            //IF no enemies are left, end the game
            else if(enemyWaves <= 0)
            {
                enemyList.splice(enemyIndex, 1);
                enemyCountText.text = 'Enemies left: ' + enemyList.length;
                objectiveCompleted = true;
            }
            //If waves are left, spawn more enemies
            else{
                enemyList.splice(enemyIndex, 1);
                spawnEnemies(enemySpawns.length, spawnPoints);
                enemyCountText.text = 'Enemies left: ' + enemyList.length;
            }
            //gameOver = true;
            /*var killEffect = game.add.sprite(enemy.body.x, enemy.body.y, 'redBoom');
            killEffect.scale.setTo(0.8,0.8);
            killEffect.alpha = 0.7;
            game.time.events.add(Phaser.Timer.SECOND * 0.3, function() {killEffect.alpha = 0.5;}, this);
            game.time.events.add(Phaser.Timer.SECOND * 0.8, function() {killEffect.kill(); }, this);*/

            //Add points to player score and update player stats and UI
            score += enemy.XP;
            scoreText.text = 'Score: ' + score;
            gainXP(enemy.XP);

        }
        //Show enemy health, testing purposes only
        else
        {
            console.log("The enemy has: " + enemy.Health + " health");
        }
    } 
}

function spawnEnemies(enemyCount, spawnPoints)
{
    //Create enemies
    for(var i = 0; i < enemyCount; i++)
    {
        var tag = 'enemy' + i;
        tag = new Enemy(this , player.level, enemySpawns[i].x, enemySpawns[i].y, 'hero');
        enemyList.push(tag);
    }
    enemyWaves -= 1;
}