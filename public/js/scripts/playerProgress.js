function gainXP(amount){
    //Increase player XP by amount
    player.XP += amount;
    renderXPnumber.text = player.XP;

    collectedXP += amount;
    lvlProgress += amount;



    if(player.XP > XlevelXP)
        {    
            console.log('levelup');
            //Increase the level by 1
            player.level++;
                
            player.skillPoints++;

            player.armor += 3;
            player.resistance += 1.5;
    
            player.damage += 2;
            player.magic += 1;
            player.maxHealth += 3;

            //Restore full health on level up
            player.Health = player.maxHealth;

            if(player.Alive){
                //Create notification for a level up
                var notification = inventory.add.text(165, 125, 'Level up!' , { fontSize: '15px', fill: '#FFFFFF' });
                game.physics.arcade.enable(notification);
                notification.body.velocity.y = 5;

                game.time.events.add(Phaser.Timer.SECOND * 1, function() {notification.alpha = 0.5;}, this);
                game.time.events.add(Phaser.Timer.SECOND * 1.1, function() {notification.alpha = 0.4;}, this);
                game.time.events.add(Phaser.Timer.SECOND * 1.2, function() {notification.alpha = 0.3;}, this);
                game.time.events.add(Phaser.Timer.SECOND * 1.3, function() {notification.alpha = 0.2;}, this);
                game.time.events.add(Phaser.Timer.SECOND * 1.4, function() {notification.alpha = 0.1;}, this);
                game.time.events.add(Phaser.Timer.SECOND * 1.5, function() {notification.alpha = 0;}, this);
            }
            else
            {
                var lvlNotification = inventory.add.text(580, 290, "Leveled up to " + player.level, { fontSize: '10px', fill: '#FFFFFF' });
            }

            playerlevelText.text = 'Level: ' + player.level;
            
            /*lvlNotification.fixedToCamera = true;
            lvlNotification.alpha = 1;

            game.time.events.add(Phaser.Timer.SECOND * 4, function() {lvlNotification.alpha = 0;}, this);*/

            //Update the total XP needed for the next level
            XlevelXP = Math.pow(player.level,2) * 100 + (player.level * 25);
            
            xpLEFT = countDifference(XlevelXP, player.XP);
            
            PrevLevelXP = (player.level-1) * 100 + ((player.level-1) * 25);
            
            tillNextLVL = countDifference(XlevelXP,  PrevLevelXP);
            
            lvlProgress = countDifference(tillNextLVL, xpLEFT);  
                    
        }
        updateStats();
       
}

function countDifference(a, b) {return a - b};

