function GameOver(){
    gameOver = false;
    player.kill();
    player.Alive = false;

    var gameOverText = game.add.text(500, 150, 'You died!', uiStyle);
        
    gameOverText.fixedToCamera = true;
}

function startOver(){

}

function spawnExit(posX, posY){
    //End of round text
    var uistyle = { font: "bold 20px Arial", fill: "#fff", boundsAlignH: "left", boundsAlignV: "top" };
    var endtext = game.add.text(0, 100,  '', uistyle);
        
    endtext.setTextBounds(25, 0, 1200, 0);
    endtext.fixedToCamera = true;
    endtext.text = "Challenge complete, find the exit";

    game.time.events.add(Phaser.Timer.SECOND * 3,  function() {endtext.text = '';}, this);
        
    var exitDoor = exitPoints.create(posX, posY, 'window');
    exitDoor.immovable = true;
}

function levelFinished(){
    console.log('You passed the level');

    //Add XP to player before saving
    var reward = 150;

    //Kill the sprite so they can't move after entering exit
    player.kill();
    player.Alive = false;

    //Hide UI
    /*
    scoreText.alpha = 0;
    enemyCountText.alpha = 0;

    
    renderHealthnumber.alpha = 0;
    renderXPnumber.alpha = 0;
    
    game.myHealthBar.kill();
    game.XPBar.kill();*/

    /* XP BAR settings */
    var XPbarConfig = {x: 625, y: 325, width:300, height:30, bg:{color:'#808080'}, bar:{color:'#c6c914'}};
    game.XPBar = new HealthBar(this.game, XPbarConfig);
                
    var fixedToCamera = true;
    game.XPBar.setFixedToCamera(fixedToCamera);
    game.XPBar.fixedToCamera = true;

    //XPbar update
    game.XPBar.setPercent(0);
            
    renderXPnumber = game.add.text(610, 320, player.XP, { fontSize: '10px', fill: '#ffffff' });
    renderXPnumber.fixedToCamera = true;
    renderXPnumber.alpha = 1;   

    centerNotification.text = '';
    //End of round text
    var uistyle = { font: "bold 20px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
    var endtext = game.add.text(0, 100,  '', uistyle);
    
    endtext.setTextBounds(25, 100, 1200, 100);
    endtext.fixedToCamera = true;
    endtext.text = "You survived!\nYou gained " + collectedXP + " + " + reward +" XP!";

    //Menu
    var choice1 = game.add.button(475,350, 'continueButton', function(){
        changeLevel('safeland')
    }) 
    choice1.fixedToCamera = true;

    gainXP(reward);

    //XPbar update
    game.time.events.add(Phaser.Timer.SECOND * 0.5,  function() {game.XPBar.setPercent(player.XP*100/XlevelXP);}, this);
    
    
    //Save game, using cookies to store player stats
    saveGame();
}


function changeLevel(state){
    game.state.start(state);   
}

function enterArea(player,door)
{
    changeState(door.name);
}