function setCookie(cname, cvalue, exdays)
{
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
    
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname)
{
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function checkCookie()
{
  var cookieExists = getCookie("saveExists");
  
    if(cookieExists)
    {
        saveExists = true;
    }
    else{
        saveExists = false;
    }

}


function saveGame()
{
    //For easier cookie checkup, let's create a boolean cookie which tells us whetever we have a save or not
    saveExists = true;
    
    setCookie('saveExists', saveExists, 365);

    setCookie('buildVersion', buildVersion, 365);
    
    //Get player stats
    playerStats = [player.XP, player.level, player.skillPoints, player.type, player.Health, player.armor, player.resistance, player.damage, player.magic, player.maxHealth, score];
  
    //Turn the array into json array so we can add it to a cookie
    var stats_arr = JSON.stringify(playerStats);
    
    //Create the cookie
    setCookie('playerStat', stats_arr, 365);

    var inv = [];
    if(inventoryItems.children.length > 0)
    {

    }
    for(var i = 0; i < inventoryItems.children.length; i++)
    {
      var Item = {};
      Item.damage = inventoryItems.children[i].damage;
      Item.key = inventoryItems.children[i].key;
      Item.slot = inventoryItems.children[i].slot;
      Item.name = inventoryItems.children[i].name;
      Item.type = inventoryItems.children[i].type;
      inv.push(Item);
    }

    //Turn into json array
    var inventory_arr = JSON.stringify(inv);

    //Create the cookie
    setCookie('playerInventory', inventory_arr, 365);

    //Equipped items

    var equipped = [];
    for(var i = 0; i < equipmentSprites.children.length; i++)
    {
        var Item = {};
        Item.name = equipmentSprites.children[i].name;
        Item.slot = equipmentSprites.children[i].slot;
        Item.key = equipmentSprites.children[i].key;
        Item.type = equipmentSprites.children[i].type;

        if(Item.type == "weapon")
        {
          Item.damage = equipmentSprites.children[i].damage;
          Item.magic = equipmentSprites.children[i].magic;
        }
        else if(Item.type == "chest" ||Item.type == "pants" || Item.type == "boots")
        {
          Item.armor = equipmentSprites.children[i].armor;
        }
        equipped.push(Item);
    }

    //Turn into json array
    var equipment_arr = JSON.stringify(equipped);

    //Create the cookie
    setCookie('playerEquipment', equipment_arr, 365);
    
        
}

/* Cookie handling */
function loadGame()
{
    var version = getCookie('buildVersion');

    if(version != buildVersion || version == undefined || version == ""){
      delete_cookie();
    }
    else
    {
      var statc = getCookie('playerStat');
      var inventory = getCookie('playerInventory');
      var equipment = getCookie('playerEquipment');

      if(inventory != undefined)
      {
        loadedInventory = JSON.parse(inventory);
      }

      if(equipment != undefined)
      {
        loadedEquipment = JSON.parse(equipment);
      }
  
      playerStats = JSON.parse(statc);
    }
}

function delete_cookie() {
  document.cookie = "playerStat=; expires=Thu, 01 Jan 1970 12:00:00 UTC; path=/";
  document.cookie = "saveExists=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
  document.cookie = "playerInventory=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
  var save1 = getCookie("playerStat");
  var save2 = getCookie("saveExists");
  var save3 = getCookie("playerInventory");
  console.log(save1, save2, save3);
}