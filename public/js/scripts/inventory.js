//inventoryArray contains the actual objects with stats

//inventoryItems is used to display sprites on the screen
//inventorySlots is a tilemap to place sprites correctly in the inventory

//Autoloot mech
function autoLoot(player, item){
    //Then find empty slot in the tilemap where we render the item
    var emptySlot = inventorySlots.children.findIndex(function(point)
    {
        return point.taken == false && point.name == "inventorySlot";
    })
    
    if(emptySlot != -1 && emptySlot <= inventoryLimit-1)
    {
            //Then find empty slot in the tilemap where we render the item
            var emptySlot = inventorySlots.children.findIndex(function(point)
            {
                return point.taken == false && point.name == "inventorySlot";
            })
            
            if(emptySlot != -1 && emptySlot <= inventoryLimit-1)
            {
            var x = inventorySlots.children[emptySlot].x;
            var y = inventorySlots.children[emptySlot].y;

            var Item = inventoryItems.create(x+1 ,y+50, item.key);
            //var Item = {};
            if(item.type == "armor")
            {
                Item.armor = item.armor;
            }
            if(item.type == "weapon")
            {
                Item.damage = item.damage;
            }
    
            Item.key = item.key;
            Item.name = item.name;
            Item.type = item.type;
            Item.slot = emptySlot;

            //inventoryArray.push(Item);
            item.kill();
                
            inventorySlots.children[emptySlot].taken = true;
        }
    }
    else
    {
        console.log('Inventory full')
    }
}

//Pickup on click
function itemPickUp(item){
        var xDistance;
        var yDistance;
        if(item.body.x > player.body.x)
        {
            xDistance = item.body.x - player.body.x;
        }
        else
        {
            xDistance = player.body.x - item.body.x;
        }

        if(item.body.y > player.body.y)
        {
            yDistance = item.body.y - player.body.y;
        }
        else
        {
            yDistance = player.body.y - item.body.y;
        }
        if(xDistance < 75 && yDistance < 75)
        {
            //Then find empty slot in the tilemap where we render the item
            var emptySlot = inventorySlots.children.findIndex(function(point)
            {
                return point.taken == false && point.name == "inventorySlot";
            })
            
            if(emptySlot != -1 && emptySlot <= inventoryLimit-1)
            {
                var x = inventorySlots.children[emptySlot].x;
                var y = inventorySlots.children[emptySlot].y;
                var Item = inventoryItems.create(x+1 ,y+50, item.key);
                if(item.type == "chest" ||item.type == "pants" || item.type == "boots" || item.type == "helmet")
                {
                    Item.armor = item.armor;
                }
                if(item.type == "weapon")
                {
                    Item.damage = item.damage;
                    Item.magic = item.magic;
                }
    
                Item.key = item.key;
                Item.name = item.name;
                Item.type = item.type;
                Item.slot = emptySlot;
                
                item.destroy();
                if(inventoryState == "equipment")
                {
                    Item.kill();
                }
                
                inventorySlots.children[emptySlot].taken = true;
            }
            else
            {
                console.log('Inventory full')
            }
        }
        else{
            console.log("Item too far");
        }
}

function loadInventory(loadeditems){

    for(var i = 0; i < loadeditems.length; i++){
        if(inventorySlots.children[loadeditems[i].slot].taken == true)
        {
            //Then find empty slot in the tilemap where we render the item
            var emptySlot = inventorySlots.children.findIndex(function(point)
            {
                return point.taken == false;
            })
        }
        else{
            var emptySlot = loadeditems[i].slot;
        }

        var x = inventorySlots.children[emptySlot].x;
        var y = inventorySlots.children[emptySlot].y;

        var Item = inventoryItems.create(x+1 ,y+50, loadeditems[i].key);

        inventorySlots.children[emptySlot].taken = true;

        if(loadeditems[i].type == "chest" ||loadeditems[i].type == "pants" || loadeditems[i].type == "boots" || loadeditems[i].type == "helmet")
        {
            Item.armor = loadeditems[i].armor;
        }
        if(item.type == "weapon")
        {
            Item.damage = loadeditems[i].damage;
            Item.magic = loadeditems[i].magic;
        }

        Item.key = loadeditems[i].key;
        Item.name = loadeditems[i].name;
        Item.type = loadeditems[i].type;
        Item.slot = emptySlot;
    }

}

function loadEquipment(loadedEquipment){
    for(var i = 0; i < loadedEquipment.length; i++){
        if(equipmentSlots.children[loadedEquipment[i].slot].taken == true)
        {
            //Then find empty slot in the tilemap where we render the item
            var emptySlot = equipmentSlots.children.findIndex(function(point)
            {
                return point.taken == false;
            })
        }
        else{
            var emptySlot = loadedEquipment[i].slot;
        }

        var x = equipmentSlots.children[emptySlot].x;
        var y = equipmentSlots.children[emptySlot].y;

        var Item = equipmentSprites.create(x-5 ,y+92, loadedEquipment[i].key);

        equipmentSlots.children[emptySlot].taken = true;

        if(loadedEquipment[i].type == "chest" ||loadedEquipment[i].type == "pants" || loadedEquipment[i].type == "boots" || loadedEquipment[i].type == "helmet")
        {
            Item.armor = loadedEquipment[i].armor;
        }
        if(loadedEquipment[i].type == "weapon")
        {
            Item.damage = loadedEquipment[i].damage;
            Item.magic = loadedEquipment[i].magic;
        }

        Item.key = loadedEquipment[i].key;
        Item.name = loadedEquipment[i].name;
        Item.type = loadedEquipment[i].type;
        Item.slot = emptySlot;

        Item.kill();
    }    

}
function itemClicked(item, pointer){
    //IF rightclicked, drop the item
    if(pointer.rightButton.isDown)
    {
        inventorySlots.children[item.slot].taken = false;

        var spriteIndex = inventoryItems.children.findIndex(function(point)
        {
            return point.name == item.name;
        })

        if(lastDir == 1)
        {
            var distance = 50;
        }
        else{
            var distance = -50;
        }
        itemCounter++;
        var droppedItem = "item"+ itemCounter;

        droppedItem = lootableItems.create(player.body.x+distance, player.body.y-25, item.key);
        droppedItem.name = "item"+ itemCounter;
        droppedItem.type = item.type;
        if(item.type == "chest" ||item.type == "pants" || item.type == "boots")
        {
            droppedItem.armor = item.armor;
        }
        if(item.type == "weapon")
        {
            droppedItem.damage = item.damage;
        }

        droppedItem.body.collideWorldBounds = true;
        droppedItem.body.velocity.y = 50;
        droppedItem.body.velocity.x = distance;

        item.kill();
    
        game.time.events.add(Phaser.Timer.SECOND * 0.5, function() {droppedItem.body.velocity.x = distance/2}, this);
        game.time.events.add(Phaser.Timer.SECOND * 0.75, function() {droppedItem.body.velocity.x = distance/2}, this);
        game.time.events.add(Phaser.Timer.SECOND * 1, function() {droppedItem.body.velocity.x = distance/2}, this);
        game.time.events.add(Phaser.Timer.SECOND * 1.25, function() {droppedItem.body.velocity.x = distance/2}, this);
        game.time.events.add(Phaser.Timer.SECOND * 1.5, function() {droppedItem.body.velocity.x = 0}, this);
    }

    //If leftclicked, equip the item
    if(pointer.leftButton.isDown)
    {
        //Then find empty slot in the tilemap where we render the item
        var emptySlot = equipmentSlots.children.findIndex(function(point)
        {
            return point.taken == false && point.equipmentType == item.type;
        })

        console.log(item.armor);
        console.log('Equipping to slot: ' + emptySlot);

        if(emptySlot != -1)
        {
            var x = equipmentSlots.children[emptySlot].x;
            var y = equipmentSlots.children[emptySlot].y;

            var Item = equipmentSprites.create(x-5 ,y+92, item.key);
            //var Item = {};
            if(item.type == "chest" ||item.type == "pants" || item.type == "boots" || item.type == "helmet")
            {
                Item.armor = item.armor;
                player.armor += Item.armor;
            }
            if(item.type == "weapon")
            {
                Item.damage = item.damage;
                Item.magic = item.magic;
                player.damage += Item.damage;
                player.magic += Item.magic;
            }

            Item.key = item.key;
            Item.name = item.name;
            Item.type = item.type;
            Item.slot = emptySlot;
            
            inventorySlots.children[item.slot].taken = false;
            equipmentSlots.children[emptySlot].taken = true;

            item.destroy();
            Item.kill();

        }
        else{
            console.log("Equipment full");
        }
        updateStats();
    }

}

function unEquip(item, pointer){
    //If leftclicked, equip the item
if(pointer.leftButton.isDown)
{
    //Then find empty slot in the tilemap where we render the item
    var emptySlot = inventorySlots.children.findIndex(function(point)
    {
        return point.taken == false && point.name == "inventorySlot";
    })
    console.log("Unequipping to slot: " + emptySlot);

    if(emptySlot != -1 && emptySlot <= inventoryLimit-1)
    {
        var x = inventorySlots.children[emptySlot].x;
        var y = inventorySlots.children[emptySlot].y;

        var Item = inventoryItems.create(x+1 ,y+50, item.key);
        if(item.type == "chest" ||item.type == "pants" || item.type == "boots" || item.type == "helmet")
        {
            Item.armor = item.armor;
            player.armor -= Item.armor;
        }
        else if(item.type == "weapon")
        {
            Item.damage = item.damage;
            Item.magic = item.magic;
            player.damage -= Item.damage;
            player.magic -= Item.magic;
        }

        Item.key = item.key;
        Item.name = item.name;
        Item.type = item.type;
        Item.slot = emptySlot;
        
        inventorySlots.children[emptySlot].taken = true;
        equipmentSlots.children[item.slot].taken = false;

        updateStats();

        item.destroy();
        Item.kill();
    }
    else
    {
        console.log('Inventory full')
    }  
}
}

function updateStats(){
    if(inventoryState == "inventory")
    {
        stats.alpha = 0;
        weaponStats.alpha = 0;
        helmetStats.alpha = 0;
        chestStats.alpha = 0;
        pantsStats.alpha = 0;
        bootsStats.alpha = 0;
    }
    else if(inventoryState == "equipment")
    {
        stats.alpha = 1;
        weaponStats.alpha = 1;
        helmetStats.alpha = 1;
        chestStats.alpha = 1;
        pantsStats.alpha = 1;
        bootsStats.alpha = 1;
    }
    text1 = "Player XP: " + player.XP;
    text2 = "\nPlayer level: " + player.level;
    text3 = "\nPlayer health: " + player.maxHealth;
    text4 = "\nPlayer armor: " + player.armor;
    text5 = "\nPlayer resitance: " + player.resistance;
    text6 = "\nPlayer damage: " + player.damage;
    text7 = "\nPlayer magic: " + player.magic;
    stats.text =  text1 + text2+text3+text4+text5+text6+text7;

    if(equipmentSlots.children[0].taken)
    {       
        var index = equipmentSprites.children.findIndex(function(point)
        {
            return point.type == 'weapon';
        })
        weapontext = "Weapon damage:    " + equipmentSprites.children[index].damage;
        var weaponMagic = "\nWeapon magic:   " + equipmentSprites.children[index].magic;
        weaponStats.text = weapontext + weaponMagic;
    }
    else{
        weaponStats.text = "No item equipped";
    }

    if(equipmentSlots.children[1].taken)
    {
        var index = equipmentSprites.children.findIndex(function(point)
        {
            return point.type == 'helmet';
        })
        var helmetText = "Helmet armor:    " + equipmentSprites.children[index].armor;
        helmetStats.text = helmetText;
    }
    else{
        helmetStats.text = "No item equipped";
    }

    if(equipmentSlots.children[2].taken)
    {
        var index = equipmentSprites.children.findIndex(function(point)
        {
            return point.type == 'chest';
        })
        var chestText = "Chest Armor:    " + equipmentSprites.children[index].armor;
        chestStats.text = chestText;
    }
    else{
        chestStats.text = "No item equipped";
    }

    if(equipmentSlots.children[3].taken)
    {
        var index = equipmentSprites.children.findIndex(function(point)
        {
            return point.type == 'pants';
        })
        var pantsText = "Pants armor:    " + equipmentSprites.children[index].armor;
        pantsStats.text = pantsText;
    }
    else{
        pantsStats.text = "No item equipped";
    }

    if(equipmentSlots.children[4].taken)
    {
        var index = equipmentSprites.children.findIndex(function(point)
        {
            return point.type == 'boots';
        })
        var bootText = "Boots armor:    " + equipmentSprites.children[index].armor;
        bootsStats.text = bootText;
    }
    else{
        bootsStats.text = "No item equipped";
    }
    

    
}
