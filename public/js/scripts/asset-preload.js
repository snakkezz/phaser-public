/* Preloaders */
function loadAssets(target){
    //Menu and buttons
    target.load.image('continueButton', 'assets/sprites/menu/buttonContinue.png');

    //Chars
    target.load.spritesheet('hero', 'assets/spritesheets/player1.png', 32, 48);

    //Tilemaps
    target.load.tilemap('basicdebug', 'assets/tilemaps/debug/basictest.json', null, Phaser.Tilemap.TILED_JSON);
    target.load.tilemap('safeland', 'assets/tilemaps/safeland/safeland.json', null, Phaser.Tilemap.TILED_JSON);
    target.load.tilemap('area2', 'assets/tilemaps/area2/area2.json', null, Phaser.Tilemap.TILED_JSON);
    target.load.tilemap('area3', 'assets/tilemaps/area3/area3.json', null, Phaser.Tilemap.TILED_JSON);
    
    //Tilemap assets
    target.load.image('grass', 'assets/tilemaps/png/grass.png');
    target.load.image('houseDarkAlt', 'assets/tilemaps/png/houseDarkAlt.png');
    target.load.image('houseDarkAlt2', 'assets/tilemaps/png/houseDarkAlt2.png');
    target.load.image('houseDark', 'assets/tilemaps/png/houseDark.png');
    target.load.image('houseLeft', 'assets/tilemaps/png/houseLeft.png');
    target.load.image('houseRight', 'assets/tilemaps/png/houseRight.png');
    target.load.image('houseTop', 'assets/tilemaps/png/houseTop.png');
    target.load.image('houseTopLeft', 'assets/tilemaps/png/houseTopLeft.png');
    target.load.image('houseTopRight', 'assets/tilemaps/png/houseTopRight.png');
    target.load.image('roundRoad', 'assets/tilemaps/png/roundRoad.png');

    //Items
    target.load.image('sword1', 'assets/inventory/sword1.png');
    target.load.image('staff1', 'assets/inventory/staff1.png');
    target.load.image('bluestaff', 'assets/inventory/bluestaff.png');
    target.load.image('redstaff', 'assets/inventory/redstaff.png');
    target.load.image('yellowstaff', 'assets/inventory/yellowstaff.png');
    target.load.image('purplestaff', 'assets/inventory/purplestaff.png');
    target.load.image('kukka', 'assets/inventory/kukka.png');
    target.load.image('kissankello', 'assets/inventory/kissankello.png');
    target.load.image('leatherGloves', 'assets/inventory/leatherGloves.png');
    target.load.image('leatherPants', 'assets/inventory/leatherPants.png');
    target.load.image('leatherChest', 'assets/inventory/leatherChest.png');
    target.load.image('leatherBoots', 'assets/inventory/leatherBoots.png');

    //Map objects
    target.load.image('window', 'assets/sprites/window.png');

    //Player objects
    target.load.image('fireball','assets/sprites/fireball.png');
}

function loadObjectGroups(){
    playerShots = game.add.group();
    playerShots.enableBody = true;
    
    playerShots.physicsBodyType = Phaser.Physics.ARCADE;
    playerShots.createMultiple(50, 'fireball');
    playerShots.setAll('checkWorldBounds', true);
    playerShots.setAll('outOfBoundsKill', true);

    enemies = game.add.group();
    enemies.enableBody = true;
    enemies.physicsBodyType = Phaser.Physics.ARCADE;
    
    enemyShots = game.add.group();
    enemyShots.enableBody = true;
    
    enemyShots.physicsBodyType = Phaser.Physics.ARCADE;
    enemyShots.createMultiple(50, 'fireball');
    enemyShots.setAll('checkWorldBounds', true);
    enemyShots.setAll('outOfBoundsKill', true);

    exitPoints = game.add.group();
    exitPoints.enableBody = true;
    exitPoints.physicsBodyType = Phaser.Physics.ARCADE;

    spawnPoints = game.add.group();

    lootableItems = game.add.group();
    lootableItems.enableBody = true;
    game.physics.arcade.enable(lootableItems);
    lootableItems.setAll('body.gravity.y', 100);
    lootableItems.inputEnableChildren = true;
}

function loadUIelements(){
    //Text styling
    uiStyle = {fontSize: '20px' , fill: '#FFFFFF'};

    var cstyle = { font: "bold 32px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };

    
    /* SCOREBOARD */
    if(!scoreText)
    {
        scoreText = inventory.add.text(25, 15, 'Score: ' + score, uiStyle);
        scoreText.fixedToCamera = true;
    }
    else{
        scoreText.alpha = 1;
        scoreText.text = 'Score: ' + score;
    }


    /*ENEMY COUNT*/
    if(!enemyCountText)
    {
        enemyCountText = inventory.add.text(245, 15, 'Enemies left: ' + enemyList.length, uiStyle);
        enemyCountText.fixedToCamera = true;
    }
    else{
        enemyCountText.alpha = 1;
        enemyCountText.text = 'Enemies left: ' + enemyList.length;
    }


    var style2 = {fontSize: '15px' , fill: '#FFFFFF'};
    if(!playerlevelText)
    {
        playerlevelText = inventory.add.text(165, 95, 'Level: ' + player.level, style2);
        playerlevelText.fixedToCamera = true;
    }
    else{
        playerlevelText.alpha = 1;
        playerlevelText.text = 'Level: ' + player.level;
    }

    var statStyle = { font: "bold 12px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
    stats = inventory.add.text(30, 260, '', statStyle);

    weaponStats = inventory.add.text(100, 440, '', statStyle);
    helmetStats = inventory.add.text(100, 500, '', statStyle);
    chestStats = inventory.add.text(100, 580, '', statStyle);
    pantsStats = inventory.add.text(100, 650, '', statStyle);
    bootsStats = inventory.add.text(100, 720, '', statStyle);

    /* Health bar settings */
    var healthBarConfig = {x: 200, y: 80, width:300, height:20, bg:{color:'#808080'}, bar:{color:'#FF0000'}};
    game.myHealthBar = new HealthBar(this.inventory, healthBarConfig);

    /*var fixedToCamera = true;
    game.myHealthBar.setFixedToCamera(fixedToCamera);
    game.myHealthBar.fixedToCamera = true;*/            
 
    renderHealthnumber = inventory.add.text(180, 70, player.Health, { fontSize: '18px', fill: '#FFFFFF' });
    renderHealthnumber.fixedToCamera = true;
    renderHealthnumber.alpha = 1;

    /* XP BAR settings */
    var XPbarConfig = {x: 200, y: 120, width:300, height:10, bg:{color:'#808080'}, bar:{color:'#e65c00'}};
    game.XPBar = new HealthBar(this.inventory, XPbarConfig);
       
    /*
    var fixedToCamera = true;
    game.XPBar.setFixedToCamera(fixedToCamera);
    game.XPBar.fixedToCamera = true;*/
            
    renderXPnumber = inventory.add.text(185, 115, player.XP, { fontSize: '9px', fill: '#FFFFFF' });
    renderXPnumber.fixedToCamera = true;
    renderXPnumber.alpha = 1;    


    /* NOTIFICATION MESSAGE*/

    leftNotification = game.add.text(0, 100,  '', cstyle);
    leftNotification.setTextBounds(0, 100, 150, 100);
    leftNotification.fixedToCamera = true;

    centerNotification = game.add.text(0, 100, '', cstyle);
    centerNotification.setTextBounds(0, 100, 1200, 100);
    //(0, 100, 1200, 100);
    centerNotification.fixedToCamera = true;

    rightNotification = game.add.text(0, 100,  '', cstyle);
    rightNotification.setTextBounds(500, 100, 1200, 100);
    rightNotification.fixedToCamera = true;
}

function resetVariables(){
    //Let's set base variables incase of conflicts
    //score = 0;
    itemCounter = 0;
    enemyID = 0;
    gameOver = false;
    objectiveCompleted = false;
    exitSpawned = false;
    levelPassed = false;

    //Let's reset our enemy array so there aren't any objects before we create one
    spawnPoints = [];
    enemyList = [];
    enemySpawns = [];
}
