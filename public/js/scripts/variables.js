//Players variables
var player, cursors, lastDir;
var shotoncooldown = false;
var lvlProgress, XlevelXP, tillNextLVL; // variables for healthbar/xp bar updating

//Enemy variables
var enemy, enemyOne, enemyTwo;
var enemyID;

//Game variables
var gameOver, objectiveCompleted, exitSpawned, levelPassed;

var targetLevel;

var collectedXP = 0;

//Cookie variables
var saveExists, playerStats, playerInventory, loadedInventory, loadedEquipment;

var buildVersion = 0.31;


//Map variables
var platforms;
var exitX, exitY;
var spawnPoints = [];
var enemySpawns = [];
var enemyList = [];
var enemyWaves;

var inventoryArray = [];

var inventoryLimit = 24;

var inventoryOn = false;
var inventoryState;

var equippedItems = [];


//UI variables
var score, scoreText, enemyCount, enemyCountText, playerlevelText, stats, weaponStats, helmetStats, chestStats, pantsStats,bootsStats;

var text1 = "Player health: ";
var text2 = "\nPlayer armor: ";
var text3 = "\nPlayer resitance: ";
var text4 = "\nPlayer damage: ";
var text5 = "\nPlayer magic: ";
var text6 = "\nPlayer XP: ";
var text7  = "\nPlayer Level: ";

var leftNotification, centerNotification, rightNotification;

//Hotkey variables
var moveLeft = Phaser.Keyboard.A;
var moveRight = Phaser.Keyboard.D;
var moveDown = Phaser.Keyboard.S;
var moveUp = Phaser.Keyboard.SPACEBAR;
var moveUp2 = Phaser.Keyboard.W
var shift = Phaser.Keyboard.SHIFT;

var shootHotkey = Phaser.Keyboard.ONE;