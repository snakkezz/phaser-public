//Create a new game
var game = new Phaser.Game(1000, 832, Phaser.CANVAS, 'gameScreen');
game.physics = 'arcade';

//Add maps
game.state.add('mainmenu', rpg.state0); //Main menu
game.state.add('debug', rpg.state1); //debug level
game.state.add('safeland', rpg.state2); //safeland where player can customize their character
game.state.add('area2', rpg.state3);
game.state.add('area3', rpg.state4);

//Create Inventory canvas
var inventory = new Phaser.Game(416, 832, Phaser.CANVAS, 'inventoryScreen');
inventory.physics = 'arcade';

//Add states for the inventory
inventory.state.add('inventory', rpgInv.state0);
//Start game
changeLevel('debug');
