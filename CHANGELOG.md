### Changelog

### 20.1.2020 Updated changelog and work tree

Development build pushed to live once a week  

-------------------------------------------------
Version 0.1  

### 22.1.2020 Level design
**Notes**

* Maps can be completed by killing remaining enemies and finding the exit door
* 2 test maps (placeholders)
* "Safeheaven" where player may upgrade their character (not implemented yet)

-------------------------------------------------
Version 0.2    

### 26.1.2020 AI testing  
**Notes**  

* Added patrolling enemies  


### 19.2.2020 Spawn testing  
**Notes**  

* Dynamic spawning for enemies

### 21.2.2020 
**Notes**

* Bugfixes for spawning and patrolling enemies
* New layout for safeheaven, 3 available maps


### 22.2.2020
**Notes**

* UI cleanup
* UI is now on a different canvas

### 23.2.2020
**Notes**

* Items can now be picked up into inventory

-------------------------------------------------
Version 0.3

### 27.2.2020
**Notes**

* Items can be dropped from the inventory and picked up again.

### 28.2.2020
**Notes**

* Items are can be now picked up by clicking rather than walking over them. Autoloot mech will be available to be enabled later on if player choices so.
* New inventory UI changes
* Level up notifications
* Dropped items are dropped behind the player rather than in front of them

### 4.3.2020
**Notes**

* Items can be now equipped by right-clicking them on the inventory and unequipping the equipped items. They increase player's stats.
* Inventory is now saved in a cookie
* Cookie saving fixes, items and equipment are now correctly stored. In case of any errors, use "delete cookies" button for clearing cookies properly.
* Equipment and inventory are now in different windows, switching happens using inventory buttons.
* Player's and equipment's stats can now be seen at equipment window