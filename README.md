# Phaser project

## 1/2020 update

This is a open source build for 2D platformer/survival, built in PhaserJS.  

The project started as an assignment for a game development course and I continued development afterwards as a hobby project. I worked on it for 2 months during 2019 summer, before the next semester started, but put the project on hold at that point.

~~I am now continuing this project as of 1/2020, working on it as a part of another game development course.  
Current plan is to push changes once a week to the live server. Check out the link below if you wish to test it yourself.~~  


~~Current live version: **0.3 | 4.3.2020**  
[Version 0.3](https://snakkezz.gitlab.io/phaser-public/)~~

~~See full set of changes here  
[Changelog](https://gitlab.com/snakkezz/phaser-game-project/blob/master/CHANGELOG.md)~~  

### This project is now continued as a team project [here](https://gitlab.labranet.jamk.fi/team-rogue/game)

Feel free to send me feedback and suggestions to saku.tupala@outlook.com.  

Phaser website  
[https://phaser.io/](https://phaser.io/)

See my portfolio here  
[http://www.sakutupala.com/](http://www.sakutupala.com/)
